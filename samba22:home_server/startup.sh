#! /bin/bash
# @edt ASIX M06 2022-2022
# startup.sh
# -------------------------------------

# Creació de xixa per als shares
# Share public
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

usuaris_ldap=$(ldapsearch -x -LLL -h ldap.edt.org -b "dc=edt,dc=org" uid | grep uid: | cut -d" " -f2)
for usuari in $usuaris_ldap
do
	home=$(ldapsearch -x -LLL -h ldap.edt.org -b "dc=edt,dc=org" uid=$usuari homeDirectory| grep homeDirectory |cut -d" " -f2)
	gidnumber=$(ldapsearch -x -LLL -h ldap.edt.org -b "dc=edt,dc=org" uid=$usuari gidNumber | grep gidNumber  |cut -d" " -f2)
	grup=$(ldapsearch -x -LLL -h ldap.edt.org -b "dc=edt,dc=org"  gidNumber=$gidnumber dn | grep grups | cut -d"=" -f2 | cut -d"," -f1)
	groupadd -g $gidnumber  -f $grup

	useradd -m -s /bin/bash -d $home -G $grup  $usuari
	echo -e "$usuari\n$usuari" | passwd $usuari
	echo -e "$usuari\n$usuari" | smbpasswd -a $usuari
done


pdbedit -L

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
